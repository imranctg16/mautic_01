<?php


use Mautic\Auth\ApiAuth;

if ($_POST) {
    echo "Index PHP File ";
    // var_dump($_POST);
    $settings = array(
        'baseUrl' => $_POST['base_url'],
        'version' => 'OAuth1a',
        'clientKey' => $_POST['consumer_key'],
        'clientSecret' => $_POST['consumer_secret'],
        'callback' => 'http://127.0.0.1:8000/test/success'
    );

// Initiate the auth object
    $initAuth = new ApiAuth();
    $auth = $initAuth->newAuth($settings);
    //print_r($auth);

// Initiate process for obtaining an access token; this will redirect the user to the authorize endpoint and/or set the tokens when the user is redirected back after granting authorization
    try {
        if ($auth->validateAccessToken()) {


            // Obtain the access token returned; call accessTokenUpdated() to catch if the token was updated via a
            // refresh token

            // $accessTokenData will have the following keys:
            // For OAuth1.0a: access_token, access_token_secret, expires
            // For OAuth2: access_token, expires, token_type, refresh_token

            if ($auth->accessTokenUpdated()) {
                $accessTokenData = $auth->getAccessTokenData();
                //store access token data however you want
                echo "You are done !" . $accessTokenData;
            }
        }
    } catch (Exception $e) {
        echo "Caught Exception " . $e;

    }
}
?>
    <!doctype html>

<head>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">

    <form action="{{route('test')}}" method="post">
        @csrf

        <div class="form-group">
            <label for="base_url">Mautic Base URl :</label>
            <input type="text" class="form-control" id="base_url" name="base_url"
                   value="http://imran.teamshunno.com/mautic">
        </div>
        <div class="form-group">
            <label for="consumer_key">Consumer Key </label>
            <input type="password" class="form-control" id="consumer_key" name="consumer_key"
                   value="2m7sd895i9a8s4cgc48k88wcsoc84ow8kg4k0s84ogokwswww">
        </div>

        <div class="form-group">
            <label for="consumer_secret">Consumer Secret </label>
            <input type="password" class="form-control" id="consumer_secret" name="consumer_secret"
                   value="1q4ung7uk1b4oogo0ossg8wwgoggg0ckww44sgsg0gg40w4s4s">
        </div>

        <input type="submit" class="btn btn-success" value="Submit">
    </form>


</div>
</body>
