<?php
/**
 * Created by PhpStorm.
 * User: BS166
 * Date: 12/18/2018
 * Time: 8:07 PM
 */

try {
    if ($auth->validateAccessToken()) {


        // Obtain the access token returned; call accessTokenUpdated() to catch if the token was updated via a
        // refresh token

        // $accessTokenData will have the following keys:
        // For OAuth1.0a: access_token, access_token_secret, expires
        // For OAuth2: access_token, expires, token_type, refresh_token

        if ($auth->accessTokenUpdated()) {
            $accessTokenData = $auth->getAccessTokenData();
            //store access token data however you want
            echo "You are done !" . $accessTokenData;
        }
    }
} catch (Exception $e) {
    echo "Caught Exception " . $e;

}


?>
